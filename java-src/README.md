# Album project back-end

---
## MODEL
#### Photo
```json
{"id": 0, "name": "photo name", "comments": []} 
```
- id - unique identifier of a photo
- name - name of a photo
- image - array of bytes, binary representation of an image
- comments - array of an [comment](#Comment) objects

#### Comment
```json
{"id": 0, "comment": "text"}
```
- id - unique identifier of a comment, unaffected
- comment - text of a comment

---
## ENDPOINTS

- **GET** /photos - get all photos as array
Parameters:
    - filter - get photos with names matching specified filter. Optional parameter, default - none.
    - precise - boolean flag for a filter. If `true` - return only photos with the same name,
    otherwise - return all photos that contain a search string. Optional parameter, default - `true`.
- **POST** /photos - add new photo to an album. Default name "image" is used.  
Body:  
    - image - binary representation of an image (file content).
- **GET** /photos/`:id` - get a single photo by id specified in the path
- **PUT** /photos/`:id` - rename a photo by id.  
Body:
    - name - new name of a photo.
- **DELETE** /photos/`:id` - delete photo with specified id from an album
- **POST** /photos/`:id`/comment - add a comment for a photo.  
Body:
    - comment - text for a comment.
