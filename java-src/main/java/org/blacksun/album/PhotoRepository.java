package org.blacksun.album;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepository extends CrudRepository<Photo, Integer> {
    Iterable<Photo> findAllByName(String name);

    @Query("select p from Photo p where p.name like %:name%")
    Iterable<Photo> findAllByNameMatches(@Param("name") String name);
}
