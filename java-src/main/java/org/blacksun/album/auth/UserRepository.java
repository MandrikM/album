package org.blacksun.album.auth;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByNameAndPassword(String name, String password);
}
