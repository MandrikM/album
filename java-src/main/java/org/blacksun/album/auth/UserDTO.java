package org.blacksun.album.auth;

public class UserDTO {
    private final String name;
    private final String token;

    public UserDTO(String name, String token) {
        this.name = name;
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public String getToken() {
        return token;
    }
}
