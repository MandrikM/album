package org.blacksun.album.auth;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {
    private final UserRepository repository;
    private final AuthProvider authProvider;

    public LoginService(UserRepository repository, AuthProvider authProvider) {
        this.repository = repository;
        this.authProvider = authProvider;
    }

    public Optional<UserDTO> executeLogIn(LoginDTO login) {
        return repository.findByNameAndPassword(login.getUser(), login.getPassword())
                .map(user -> new UserDTO(user.getName(), authProvider.generateToken(user)));
    }
}
