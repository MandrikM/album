package org.blacksun.album.auth;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class UserController {
    private final LoginService loginService;

    public UserController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody LoginDTO login) {
        Optional<UserDTO> userDTO = loginService.executeLogIn(login);
        if (userDTO.isPresent()) {
            return ResponseEntity.ok(userDTO.get());
        }
        return ResponseEntity.badRequest().build();
    }
}
