package org.blacksun.album;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@CrossOrigin
@RestController
@RequestMapping("/photos")
public class PhotosController {
    private final PhotoRepository repository;
    private final CommentRepository commentRepository;

    public PhotosController(PhotoRepository repository, CommentRepository commentRepository) {
        this.repository = repository;
        this.commentRepository = commentRepository;
    }

    @GetMapping
    public Iterable<PhotoDto> getAll(@RequestParam(defaultValue = "") String filter,
                                  @RequestParam(defaultValue = "true") boolean precise) {
        final String trimmed = filter.trim();
        final Iterable<Photo> photos;
        if (trimmed.isEmpty()) {
            photos = repository.findAll();
        } else if (precise) {
            photos = repository.findAllByName(trimmed);
        } else {
            photos = repository.findAllByNameMatches(trimmed);
        }
        return StreamSupport.stream(photos.spliterator(), false)
                .map(PhotoDto::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<PhotoDto> add(@RequestBody byte[] image) throws IOException {
        final Photo photo = new Photo();
        photo.setImage(image);
        photo.setName("image");
        BufferedInputStream stream = new BufferedInputStream(new ByteArrayInputStream(image));
        photo.setContentType(URLConnection.guessContentTypeFromStream(stream));
        repository.save(photo);
        return ResponseEntity.ok(new PhotoDto(photo));
    }

    @GetMapping(value = "/{id}", produces = { MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE})
    public ResponseEntity<byte[]> get(@PathVariable int id) {
        Optional<Photo> photo = repository.findById(id);
        if (!photo.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        final byte[] data = photo.get().getImage();
        final String contentType = photo.get().getContentType();
        final MediaType type = contentType == null ? MediaType.parseMediaType("video/mp4") : MediaType.parseMediaType(contentType);
        return ResponseEntity.ok()
                .contentLength(data.length)
                .contentType(type)
                .body(data);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PhotoDto> rename(@PathVariable int id, @RequestBody String name) {
        final Optional<Photo> optPhoto = repository.findById(id);
        if (!optPhoto.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        final Photo photo = optPhoto.get();
        photo.setName(name);
        repository.save(photo);
        return ResponseEntity.ok(new PhotoDto(photo));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PhotoDto> delete(@PathVariable int id) {
        final Optional<Photo> optPhoto = repository.findById(id);
        if (!optPhoto.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        repository.deleteById(id);
        return ResponseEntity.ok(new PhotoDto(optPhoto.get()));
    }

    @PostMapping(value = "/{id}/comment")
    public ResponseEntity<PhotoDto> addComment(@PathVariable int id, @RequestBody String text) {
        final Optional<Photo> optPhoto = repository.findById(id);
        if (!optPhoto.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        final Photo photo = optPhoto.get();
        final Comment comment = new Comment();
        comment.setComment(text);
        photo.addComment(comment);
        commentRepository.save(comment);
        repository.save(photo);
        return ResponseEntity.ok(new PhotoDto(photo));
    }
}
