package org.blacksun.album;

import java.util.List;

public class PhotoDto {
    private final int id;
    private final String name;
    private final boolean isVideo;
    private final List<Comment> comments;

    public PhotoDto(Photo photo) {
        id = photo.getId();
        name = photo.getName();
        comments = photo.getComments();
        isVideo = photo.getContentType() == null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
