import config from './config'

export const photosPath = () => config.server + config.photos
export const photoPath = (id) => photosPath() + '/' + id
export const commentPath = (id) => photoPath(id) + config.comment
export const searchPath = (searchText, precise) => photosPath() + `?filter=${searchText}&precise=${precise}`
export const loginPath = () => config.server + config.login

export const error = (msg) => {
  console.log(msg)
  Error(msg)
}

export const authHeader = () => ({ Authorization: 'Bearer ' + sessionStorage.token || error('No token') })
export const authHeaders = () => ({ headers: authHeader() })

let key = 0
export const unique = () => key++
