import React, { Component } from 'react'
import { addPic } from './actions'
import { connect } from 'react-redux'

class Add extends Component {
  constructor (props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (event) {
    // eslint-disable-next-line react/prop-types
    this.props.addPic(event.target.files[0])
  }

  render () {
    return (
      <div className='header'>
        <input type='file' onChange={this.handleChange} />
      </div>
    )
  }
}

export default connect(null, { addPic })(Add)
