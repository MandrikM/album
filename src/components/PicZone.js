import React, { Component } from 'react'
import PicComponent from './PicComponent'
import { connect } from 'react-redux'
import { unique } from '../utils/api'

class PicZone extends Component {
  render () {
    return (
      <div className='grid'>
        {/* eslint-disable-next-line react/prop-types */}
        {this.props.pics.map((pic) => <PicComponent key={unique()} pic={pic} />)}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return { pics: state }
}

export default connect(mapStateToProps)(PicZone)
