export default function (state = [], action) {
  switch (action.type) {
    case 'ALL PICS':
      return action.payload

    default:
      return state
  }
}
