import React, { Component } from 'react'
import Search from './Search'
import PicZone from './PicZone'
import Add from './Add'
import LoginPage from './LoginPage'
import { connect } from 'react-redux'
import { download } from './actions'

class App extends Component {
  componentDidMount () {
    // eslint-disable-next-line react/prop-types
    sessionStorage.token && this.props.download()
  }

  render () {
    if (!sessionStorage.token) {
      return (
        <div className='LoginPic'>
          <LoginPage />
        </div>
      )
    } else {
      return (
        <div className='bigPic'>
          <Search />
          <input
            type='button' className='mainHeader' value='Logout'
            onClick={() => {
              sessionStorage.removeItem('token')
              this.setState({})
            }}
          />
          <PicZone />
          <Add />
        </div>
      )
    }
  }
}

// mapping to an empty object doesn't cause component to be re-rendered
export default connect(state => ({ state: state }), { download })(App)
