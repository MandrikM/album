/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import AddComment from './AddComment'
import { changeName, deletePic } from './actions'
import { connect } from 'react-redux'
import Popup from 'reactjs-popup'

import { photoPath, unique } from '../utils/api'

class PicComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      edit: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (event) {
    const newName = event.target['photo-name'].value
    if (newName !== '') {
      this.props.changeName(newName, this.props.pic.id)
    }
    event.preventDefault()
  }

  render () {
    function Comment (comment) {
      return <div className='lefttext' key={unique()}>{comment.comment}</div>
    }
    return (
      <div className='griddiv'>
        <h2 className='lefttext'>{this.props.pic.name}</h2>
        <input
          type='button' value='Edit' className='edit-button'
          onClick={() => this.setState({ edit: !this.state.edit })}
        />
        {
          this.props.pic.video ? (
            <video controls width='250'>
              <source src={photoPath(this.props.pic.id)} type='video/mp4' />
            </video>
          ) : (
            <Popup
              trigger={<img src={photoPath(this.props.pic.id)} className='img' alt='' />} position='right center'
              className='popup' contentStyle={{ width: '400px' }}
            >
              <img src={photoPath(this.props.pic.id)} alt='' />
            </Popup>
          )
        }
        {this.props.pic.comments.map(Comment)}
        {
          this.state.edit && (
            <div>
              <form onSubmit={this.handleSubmit}>
                <div className='lefttext'>
                  Change name:
                  <input type='text' value={this.state.value} name='photo-name' />
                </div>
                <input type='submit' value='Change' />
              </form>

              <AddComment id={this.props.pic.id} />
              <button onClick={() => this.props.deletePic(this.props.pic.id)}>
                Delete
              </button>
            </div>)
        }
      </div>
    )
  }
}

export default connect(null, {
  deletePic,
  changeName
})(PicComponent)
