import React, { Component } from 'react'
import { search } from './actions'
import { connect } from 'react-redux'

class Search extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (event) {
    // eslint-disable-next-line react/prop-types
    this.props.search(event.target['search-field'].value)
    event.preventDefault()
  }

  render () {
    return (
      <div className='header'>
        <form onSubmit={this.handleSubmit}>
          <label>
            Search pic:
            <input type='text' name='search-field' />
          </label>
          <input type='submit' value='Search' />
        </form>
      </div>
    )
  }
}

export default connect(null, { search })(Search)
