import React, { Component } from 'react'
import { addComment } from './actions'
import { connect } from 'react-redux'

class AddComment extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (event) {
    const comment = event.target['comment-field'].value
    if (comment !== '') {
      // eslint-disable-next-line react/prop-types
      this.props.addComment(comment, this.props.id)
    }
    event.preventDefault()
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type='text' name='comment-field' className='lefttext' />
          <input type='submit' value='Add comment' />
        </form>
      </div>
    )
  }
}

export default connect(null, { addComment })(AddComment)
