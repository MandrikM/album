import React, { Component } from 'react'
import { login } from './actions'
import { connect } from 'react-redux'

class LoginPage extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (event) {
    // eslint-disable-next-line react/prop-types
    this.props.login(event.target['name-field'].value, event.target['password-field'].value)
    event.preventDefault()
  }

  render () {
    return (
      <div className='Login'>
        <form onSubmit={this.handleSubmit}>
          <input type='text' defaultValue='admin' name='name-field'/>
          <input type='password' defaultValue='admin' name='password-field'/>
          <button type='submit'> send</button>
        </form>
      </div>
    )
  }
}

export default connect(null, { login })(LoginPage)
