export function download () {
  return {
    type: 'DOWNLOAD'
  }
}

export function addPic (pic) {
  return {
    type: 'ADD PIC',
    pic: pic
  }
}

export function search (text) {
  return {
    type: 'SEARCH',
    searchText: text
  }
}

export function addComment (text, id) {
  return {
    type: 'ADD COMMENT',
    commentText: text,
    id: id
  }
}

export function deletePic (id) {
  return {
    type: 'DELETE',
    id: id
  }
}

export function changeName (newName, id) {
  return {
    type: 'CHANGE NAME',
    newName: newName,
    id: id
  }
}

export function login (name, password) {
  return {
    type: 'LOGIN',
    data: {
      user: name,
      password: password
    }
  }
}

export function updateState (images) {
  return {
    type: 'ALL PICS',
    payload: images
  }
}
