import { all, put, takeEvery } from 'redux-saga/effects'
import { authHeaders, commentPath, error, loginPath, photoPath, photosPath, searchPath } from '../utils/api'
import { updateState } from './actions'

function * Pics () {
  yield takeEvery('DOWNLOAD', downloadPics)
}

function * downloadPics () {
  const pics = yield fetch(photosPath(), authHeaders())
    .then(response => response.ok ? response.json() : error(response))
  yield put(updateState(pics))
}

function * sendPic () {
  yield takeEvery('ADD PIC', sendNewPic)
}

function * sendNewPic (action) {
  yield fetch(photosPath(), {
    method: 'POST',
    body: action.pic,
    ...authHeaders()
  })
  yield downloadPics()
}

function * search () {
  yield takeEvery('SEARCH', searchPics)
}

function * searchPics (action) {
  const pics = yield fetch(searchPath(action.searchText, false), authHeaders())
    .then(response => response.json())
  yield put(updateState(pics))
}

function * addComment () {
  yield takeEvery('ADD COMMENT', addNewComment)
}

function * addNewComment (action) {
  yield fetch(commentPath(action.id), {
    method: 'POST',
    body: action.commentText,
    ...authHeaders()
  })
  yield downloadPics()
}

function * deletePic () {
  yield takeEvery('DELETE', deleteCurrentPic)
}

function * deleteCurrentPic (action) {
  yield fetch(photoPath(action.id), {
    method: 'DELETE',
    ...authHeaders()
  })
  yield downloadPics()
}

function * changeName () {
  yield takeEvery('CHANGE NAME', changePicName)
}

function * changePicName (action) {
  yield fetch(photoPath(action.id), {
    method: 'PUT',
    body: action.newName,
    ...authHeaders()
  })
  yield downloadPics()
}

function * login () {
  yield takeEvery('LOGIN', loginUser)
}

function * loginUser (action) {
  yield fetch(loginPath(), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(action.data)
  })
    .then(response => response.ok ? response.json() : error(response))
    .then(data => { sessionStorage.token = data.token })
  yield downloadPics()
}

export default function * watchAll () {
  yield all([
    Pics(),
    sendPic(),
    search(),
    addComment(),
    deletePic(),
    changeName(),
    login()
  ])
}
